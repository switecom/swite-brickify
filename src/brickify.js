/*
 * Brickify 
 * Real cascading grid layout library
 * by tech@swite.com
 */
(function ($) {

    $.fn.brickify = function (options = {}) {
        /*                                  */
        /*              FUNCTIONS           */
        /*                                  */

        /**
         * @description get column count
         */
        const getColumnsNumber = (gridSystem) => {
            let windowWidth = $(window).width();

            if (gridSystem.LG && windowWidth >= gridSystem.LG.width) {
                return gridSystem.LG.columns;
            }
            if (gridSystem.MD && windowWidth >= gridSystem.MD.width) {
                return gridSystem.MD.columns;
            }
            if (gridSystem.SM && windowWidth >= gridSystem.SM.width) {
                return gridSystem.SM.columns;
            }
            return 1;
        };

        /**
         * @description build bricks maps
         * @param {int} colNum column count
         * @return {Object}  bricks map
         */
        const initializeBricksMap = (colNum) => {
            let bricksMap = { cols: [] }, 
                j;
            for (j = 0; j < colNum; j++) {
                bricksMap.cols.push({ 
                    elements: [], 
                    height: 0 
                });
            }
            return bricksMap;
        };

        /**
         * @description set container height
         * @param {jQueryElement} container
         */
        const setContainerHeight = (container) => {
            if (container.data('colNum') === 1) {
                container.css('height', 'auto');
                return;
            }
            let bricksMap = container.data('bricksMap'),
                maxHeight = 0;

            for (let col of bricksMap.cols){
                if (col.height > maxHeight) {
                    maxHeight = col.height;
                }
            }
            container.css('height', maxHeight + 'px');
        };

        /**
         * @description set css transition on container children
         * @param {jQueryElement} container
         */
        const addCssTransition = (container, applyTransition) => {
            if (applyTransition) {
                container.children().each(function (idx, el) {
                    $(el).css({
                        WebkitTransition: 'all 0.2s ease-in-out',
                        MozTransition: 'all 0.2s ease-in-out',
                        MsTransition: 'all 0.2s ease-in-out',
                        OTransition: 'all 0.2s ease-in-out',
                        transition: 'all 0.2s ease-in-out'
                    });
                });
            }
        };

        /**
         * @param {*} bricksMap 
         * @param {*} options 
         */
        const getNextColumn = (bricksMap, options, elementIdx) => {
            if (options.preserveOrder){
                return elementIdx % bricksMap.cols.length;
            } else {
                return getFirstAvailableColumn(bricksMap);
            }
        };

        /**
         * @description get index of the smallest column in bricksMap
         * @param {*} bricksMap 
         */
        const getFirstAvailableColumn = (bricksMap) => {
            let minHeight, 
                firstAvailableCol,
                col; 
            for (col = 0; col < bricksMap.cols.length; col++) {
                if (minHeight === undefined || bricksMap.cols[col].height < minHeight ) {
                    minHeight = bricksMap.cols[col].height;
                    firstAvailableCol = col;
                }
            }
            return firstAvailableCol || 0;
        };

        /**
         * @description inizializza la struttura
         * @return {$elem} containers
         */
        const init = () => {

            jqEl.each(function(){
                let $this = $(this);
                let opts = $this.data('brickifyOptions'),
                    col, left, elMap;
                let colNum = getColumnsNumber(opts.gridSystem);
                let bricksMap = initializeBricksMap(colNum);

                $this.data('colNum', colNum);
                $this.data('bricksMap', bricksMap);

                addCssTransition($this, opts.cssTransition);

                if (colNum > 1) {
                    $this.css('position', 'relative');
                    $this.children().each(function (idx, el) {

                        col = getNextColumn(bricksMap, opts, idx); 

                        left = ((100 / colNum) * col);
                        elMap;
                        el = $(el).css({
                            'position': 'absolute',
                            'left': `${left}%`,
                            'width': `${(100 / colNum)}%`
                        });
                        elMap = {
                            el: el,
                            h: el.outerHeight(),
                            left: left,
                            top: 0
                        };
                        // if column is not empty
                        if (bricksMap.cols[col].elements.length > 0) {
                            elMap.top = bricksMap.cols[col].height;
                        }
                        el.css({ 
                            'top': elMap.top + 'px' 
                        });
                        bricksMap.cols[col].elements.push(elMap);
                        bricksMap.cols[col].height += elMap.h;
                    });
                    setContainerHeight($this);
                } else if (colNum === 1) {
                    $this.children().each(function (idx, el) {
                        el = $(el).css({
                            'width': '100%',
                            'position': 'relative',
                            'left': 'unset',
                            'top': 'unset'
                        });
                    });
                    setContainerHeight($this);
                }
                if (opts.rebuildOnResize){
                    $(window)
                        .off('resize', resizeFunction)
                        .on('resize', resizeFunction);
                }
            });

            return jqEl;
        };

        /**
         * @description remove window resize listener
         */
        const destroy = () => {
            $(window).off('resize', resizeFunction);
        };
        
        /**
         * @description exec an action
         * @param {string} actionName 
         */
        const brickifyAction = (actionName) => {
            switch (actionName) {
                case 'reload':
                    return init();
                    break;
                case 'destroy':
                    return destroy();
                    break;
                default:
                    break;
            }
        };

        const resizeFunction = ()=>{
            clearTimeout(resizeTimeout);
            resizeTimeout = setTimeout(()=>{
                let currentWindowWidth = $(window).width();
                if (windowWidth !== currentWindowWidth){
                    windowWidth = currentWindowWidth
                    init();
                }
            }, 66)
        };

        /*                                  */
        /*          INITIALIZATION          */
        /*                                  */
        let windowWidth = $(window).width();
        let resizeTimeout;
        let jqEl = this;
        switch (typeof options) {
            case 'object':
                let settings = $.extend({}, $.fn.brickify.defaults, options);
                jqEl.each(function () {
                    $(this).data('brickifyOptions', settings);
                });
                return init();
                break;
            case 'string':
                return brickifyAction(options);
                break;
            default:
                break;
        }
    };

    $.fn.brickify.defaults = {
        gridSystem: {
            'LG': {
                width: 1100,
                columns: 4
            },
            'MD': {
                width: 768,
                columns: 3
            },
            'SM': {
                width: 480,
                columns: 2
            }
        },
        rebuildOnResize: false,
        cssTransition: false,
        preserveOrder: false
    };

}(jQuery));

