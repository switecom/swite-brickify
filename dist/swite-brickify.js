'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 * Brickify 
 * Real cascading grid layout library
 * by tech@swite.com
 */
(function ($) {

    $.fn.brickify = function () {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        /*                                  */
        /*              FUNCTIONS           */
        /*                                  */

        /**
         * @description get column count
         */
        var getColumnsNumber = function getColumnsNumber(gridSystem) {
            var windowWidth = $(window).width();

            if (gridSystem.LG && windowWidth >= gridSystem.LG.width) {
                return gridSystem.LG.columns;
            }
            if (gridSystem.MD && windowWidth >= gridSystem.MD.width) {
                return gridSystem.MD.columns;
            }
            if (gridSystem.SM && windowWidth >= gridSystem.SM.width) {
                return gridSystem.SM.columns;
            }
            return 1;
        };

        /**
         * @description build bricks maps
         * @param {int} colNum column count
         * @return {Object}  bricks map
         */
        var initializeBricksMap = function initializeBricksMap(colNum) {
            var bricksMap = { cols: [] },
                j = void 0;
            for (j = 0; j < colNum; j++) {
                bricksMap.cols.push({
                    elements: [],
                    height: 0
                });
            }
            return bricksMap;
        };

        /**
         * @description set container height
         * @param {jQueryElement} container
         */
        var setContainerHeight = function setContainerHeight(container) {
            if (container.data('colNum') === 1) {
                container.css('height', 'auto');
                return;
            }
            var bricksMap = container.data('bricksMap'),
                maxHeight = 0;

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = bricksMap.cols[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var col = _step.value;

                    if (col.height > maxHeight) {
                        maxHeight = col.height;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            container.css('height', maxHeight + 'px');
        };

        /**
         * @description set css transition on container children
         * @param {jQueryElement} container
         */
        var addCssTransition = function addCssTransition(container, applyTransition) {
            if (applyTransition) {
                container.children().each(function (idx, el) {
                    $(el).css({
                        WebkitTransition: 'all 0.2s ease-in-out',
                        MozTransition: 'all 0.2s ease-in-out',
                        MsTransition: 'all 0.2s ease-in-out',
                        OTransition: 'all 0.2s ease-in-out',
                        transition: 'all 0.2s ease-in-out'
                    });
                });
            }
        };

        /**
         * @param {*} bricksMap 
         * @param {*} options 
         */
        var getNextColumn = function getNextColumn(bricksMap, options, elementIdx) {
            if (options.preserveOrder) {
                return elementIdx % bricksMap.cols.length;
            } else {
                return getFirstAvailableColumn(bricksMap);
            }
        };

        /**
         * @description get index of the smallest column in bricksMap
         * @param {*} bricksMap 
         */
        var getFirstAvailableColumn = function getFirstAvailableColumn(bricksMap) {
            var minHeight = void 0,
                firstAvailableCol = void 0,
                col = void 0;
            for (col = 0; col < bricksMap.cols.length; col++) {
                if (minHeight === undefined || bricksMap.cols[col].height < minHeight) {
                    minHeight = bricksMap.cols[col].height;
                    firstAvailableCol = col;
                }
            }
            return firstAvailableCol || 0;
        };

        /**
         * @description inizializza la struttura
         * @return {$elem} containers
         */
        var init = function init() {

            jqEl.each(function () {
                var $this = $(this);
                var opts = $this.data('brickifyOptions'),
                    col = void 0,
                    left = void 0,
                    elMap = void 0;
                var colNum = getColumnsNumber(opts.gridSystem);
                var bricksMap = initializeBricksMap(colNum);

                $this.data('colNum', colNum);
                $this.data('bricksMap', bricksMap);

                addCssTransition($this, opts.cssTransition);

                if (colNum > 1) {
                    $this.css('position', 'relative');
                    $this.children().each(function (idx, el) {

                        col = getNextColumn(bricksMap, opts, idx);

                        left = 100 / colNum * col;
                        elMap;
                        el = $(el).css({
                            'position': 'absolute',
                            'left': left + '%',
                            'width': 100 / colNum + '%'
                        });
                        elMap = {
                            el: el,
                            h: el.outerHeight(),
                            left: left,
                            top: 0
                        };
                        // if column is not empty
                        if (bricksMap.cols[col].elements.length > 0) {
                            elMap.top = bricksMap.cols[col].height;
                        }
                        el.css({
                            'top': elMap.top + 'px'
                        });
                        bricksMap.cols[col].elements.push(elMap);
                        bricksMap.cols[col].height += elMap.h;
                    });
                    setContainerHeight($this);
                } else if (colNum === 1) {
                    $this.children().each(function (idx, el) {
                        el = $(el).css({
                            'width': '100%',
                            'position': 'relative',
                            'left': 'unset',
                            'top': 'unset'
                        });
                    });
                    setContainerHeight($this);
                }
                if (opts.rebuildOnResize) {
                    $(window).off('resize', resizeFunction).on('resize', resizeFunction);
                }
            });

            return jqEl;
        };

        /**
         * @description remove window resize listener
         */
        var destroy = function destroy() {
            $(window).off('resize', resizeFunction);
        };

        /**
         * @description exec an action
         * @param {string} actionName 
         */
        var brickifyAction = function brickifyAction(actionName) {
            switch (actionName) {
                case 'reload':
                    return init();
                    break;
                case 'destroy':
                    return destroy();
                    break;
                default:
                    break;
            }
        };

        var resizeFunction = function resizeFunction() {
            clearTimeout(resizeTimeout);
            resizeTimeout = setTimeout(function () {
                var currentWindowWidth = $(window).width();
                if (windowWidth !== currentWindowWidth) {
                    windowWidth = currentWindowWidth;
                    init();
                }
            }, 66);
        };

        /*                                  */
        /*          INITIALIZATION          */
        /*                                  */
        var windowWidth = $(window).width();
        var resizeTimeout = void 0;
        var jqEl = this;
        switch (typeof options === 'undefined' ? 'undefined' : _typeof(options)) {
            case 'object':
                var settings = $.extend({}, $.fn.brickify.defaults, options);
                jqEl.each(function () {
                    $(this).data('brickifyOptions', settings);
                });
                return init();
                break;
            case 'string':
                return brickifyAction(options);
                break;
            default:
                break;
        }
    };

    $.fn.brickify.defaults = {
        gridSystem: {
            'LG': {
                width: 1100,
                columns: 4
            },
            'MD': {
                width: 768,
                columns: 3
            },
            'SM': {
                width: 480,
                columns: 2
            }
        },
        rebuildOnResize: false,
        cssTransition: false,
        preserveOrder: false
    };
})(jQuery);
//# sourceMappingURL=swite-brickify.js.map
